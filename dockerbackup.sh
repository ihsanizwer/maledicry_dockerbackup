#!/bin/bash
clear


backupfile=`uuidgen`


docker commit -p centos ~/${backupfile}


docker save -o ~/${backupfile}.tar ~/${backupfile} 
echo "Backed up docker image centos as ${backupfile}"
#Steps beyond this will be like pushing to gitlab
#docker push ~/${backupfile}